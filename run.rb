require_relative 'lib/world'
require 'stackprof'

world = World.new(22, 55)
world.populate
loop do
#  StackProf.run(mode: :cpu, out: 'tmp/stackprof-cpu-myapp.dump') do
    world.draw
    world.tick!
#  end
  sleep(0.1)
end
