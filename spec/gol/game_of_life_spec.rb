require 'spec_helper'
require 'cell'
require 'world'

describe 'game of life' do
  let(:world) { World.new }
  context 'cell utility methods' do

    subject { Cell.new(world) }

    it 'spawns relative to' do
      cell = subject.spawn_at(3, 5)
      expect(cell.is_a?(Cell)).to eq true
      expect(cell.x).to eq 3
      expect(cell.y).to eq 5
      expect(cell.world).to eq subject.world
    end

    it 'detects a neigbour to the north' do
      subject.spawn_at(0, 1)
      expect(subject.neighbours.count).to eq 1
    end

    it 'detects a neigbour to the south east' do
      subject.spawn_at(-1, -1)
      expect(subject.neighbours.count).to eq 1
    end

    it 'detects a neigbour to the north west' do
      subject.spawn_at(-1, 1)
      expect(subject.neighbours.count).to eq 1
    end

    it 'does not detect a cell that is not neighbouring' do
      subject.spawn_at(2, 2)
      expect(subject.neighbours.count).to eq 0
    end

    it 'dies' do
      subject.die!
      expect(subject.world.cells).to_not include(subject)
    end
  end

  context 'game of life rules' do

    subject { Cell.new(world) }

    before do
      world.cells[subject.to_s] = subject
    end

    it 'Rule 1: A cell with fewer than 2 live cells neighbours dies
        (under-population)' do
      expect(subject.neighbours.count).to eq 0
      world.tick!
      expect(subject).to be_dead
    end

    it 'Rule 2a: A cell with two neighbours lives' do
      subject.spawn_at(1, 1)
      subject.spawn_at(0, 1)
      world.tick!
      expect(subject).to be_alive
    end

    it 'Rule 2b: A cell with three neighbours lives' do
      subject.spawn_at(1, 1)
      subject.spawn_at(0, 1)
      subject.spawn_at(1, 0)
      world.tick!
      expect(subject).to be_alive
    end

    it 'Rule 3: A cell with more than three neighbours dies (overpopulation)' do
      subject.spawn_at(1, 1)
      subject.spawn_at(0, 1)
      subject.spawn_at(1, 0)
      subject.spawn_at(-1, 1)
      world.tick!
      expect(subject).to be_dead
    end

    it 'Rule 4: A dead cell with 3 live neighbours
    comes to live (reproduction)' do
      subject.spawn_at(1, 1)
      subject.spawn_at(0, 1)
      subject.spawn_at(1, 0)
      subject.die!
      world.tick!
      expect(subject).to be_alive
    end
  end
end
